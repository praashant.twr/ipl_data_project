# Ipl Data Project#### Dependencies
[Matplotlib](https://matplotlib.org/)The application is build using __PYTHON__ programming language and __MATPLOTLIB__ library.From the given CSV files that have details of every match and deliveries bowled in __IPL__ from the year 2008 to 2017, few problem sets were given from which by applying proper logic and using right data structure the graphs are plotted using __MATPLOTLIB__ library.## Problem Set    In this data assignment you will transform raw data from IPL into graphs that will convey some meaning / analysis. For each part of this assignment you will have 2 parts -    Code python functions that will transform the raw csv data into a data structure in a format suitable for plotting with matplotlib.    Generate the following plots ...    1. Plot the number of matches played per year of all the years in IPL.
   2. Plot a stacked bar chart of matches won of all teams over all the years of IPL.
   3. For the year 2016 plot the extra runs conceded per team.
   4. For the year 2015 plot the top economical bowlers.

