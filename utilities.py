import csv
from matplotlib import pyplot as plt


def match_id_for_season(season):
    match_id = []
    with open('matches.csv', 'r') as matches_file:
        matcher_reader = csv.DictReader(matches_file)

        for match in matcher_reader:
            if match['season'] == str(season):
                match_id.append(match['id'])
    return match_id


def plot_data(x_label, y_label, title, x_axis, y_axis):
    plt.style.use('fivethirtyeight')
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(title)
    plt.bar(x_axis, y_axis)
    plt.tight_layout()
    

def show_plot():    
    plt.show()