""" Plot the number of matches played per year of all the years in IPL."""

import csv
from matplotlib import pyplot as plt
from utilities import plot_data, show_plot


def matches_per_year():
    matches_per_season = {}
    with open('./matches.csv', 'r') as csv_file:
        matches_reader = csv.DictReader(csv_file)
        for match in matches_reader:
            if match['season'] in matches_per_season:
                matches_per_season[match['season']] += 1
            else:
                matches_per_season[match['season']] = 1
    return matches_per_season

def charting():
    year_list = {}
    year_list = matches_per_year()
    x_axis = []
    y_axis = []
    for key, value in year_list.items():
        x_axis.append(key)
        y_axis.append(value)
    plot_data('Seasons', 'Matches played', 'Matches played per season', x_axis, y_axis)
    show_plot()

def execute():
    charting()

execute()