import csv
from matplotlib import pyplot as plt
from utilities import plot_data, show_plot

def get_runs_scored_by_batsmen():
    runs_scored_by_batsman = {}
    with open('deliveries.csv') as deliveries_csv:
        deliveries_csv_reader = csv.DictReader(deliveries_csv)
        
        for delivery in deliveries_csv_reader:
            batsman = delivery['batsman']
            batsman_runs = int(delivery['batsman_runs'])
            if batsman in runs_scored_by_batsman:
                runs_scored_by_batsman[batsman] += batsman_runs
            else:
                runs_scored_by_batsman[batsman] = batsman_runs
    return runs_scored_by_batsman

def charting():
    runs_scored = get_runs_scored_by_batsmen()
    runs_by_batsman = {}
    for batsman, runs in runs_scored.items():
        runs_by_batsman[runs] = batsman
    batsman = []
    runs_by_corresponding_batsman = []
    for runs in sorted(runs_by_batsman.keys())[-10:]:
        runs_by_corresponding_batsman.append(runs)
        batsman.append(runs_by_batsman[runs])

    plot_data('batsman', 'runs scored', 'maximum runs scored by players', batsman, runs_by_corresponding_batsman)
    plt.xticks(rotation='vertical', fontsize='x-small')
    show_plot()    

def execute():
    charting()

execute()
