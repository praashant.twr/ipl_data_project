"""Plot a stacked bar chart of matches won of all teams over all the years of IPL."""

from matplotlib import pyplot as plt
import csv
plt.style.use('fivethirtyeight')

def matches_won_per_team():
    winning_record = {}
    seasons = []
    with open('./matches.csv', 'r') as matches_file:
        matches_reader = csv.DictReader(matches_file)

        for match in matches_reader:
            year = match['season']
            winner = match['winner']
            if winner in winning_record:
                if year in winning_record[winner]:
                    winning_record[winner][year] += 1
                else:
                    winning_record[winner][year] = 1
            else: 
                winning_record[winner] =  {year: 1}
            if year not in seasons:
                seasons.append(year)
        seasons.sort()

    for wins in winning_record.values():
        for year in seasons:
            if year not in wins:
                wins[year] = 0
    
    return winning_record, seasons

def charting():
    
    def add_bottom(win_set):
        set_bottom = []
        for i in seasons:
            set_bottom.append(0)
        if win_set == 0:
            return set_bottom
        for i in range(win_set):
            for j in range(len(plotable_data[i][1])):
                set_bottom[j] += plotable_data[i][1][j]
        return set_bottom
    colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k', '#469990', '#808000', '#e6beff', '#9A6324', '#bfef45', '#fabebe', '#a9a9a9', '#f58231' ]
    plotable_data = []
    winning_record = matches_won_per_team()[0]
    seasons = matches_won_per_team()[1]
    for team, wins in winning_record.items():
        team_with_wins = []
        team_with_wins.append(team)
        wins_of_team = []
        for win in sorted(wins):
            wins_of_team.append(wins[win])
        team_with_wins.append(wins_of_team)
        plotable_data.append(team_with_wins)

    for win_set in range(len(plotable_data)):
        plt.bar(seasons, plotable_data[win_set][1], bottom=add_bottom(win_set), label=plotable_data[win_set][0], color=colors[win_set])

    plt.legend(loc='upper right', ncol=5, fontsize='x-small')
    plt.ylim(0, 100)
    plt.show()

def execute():
    charting()

execute()
