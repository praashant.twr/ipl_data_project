""" For the year 2016 plot the extra runs conceded per team."""

import csv
from utilities import match_id_for_season
from utilities import plot_data, show_plot
from matplotlib import pyplot as plt


def extra_runs_scored_per_team():
    match_ids = match_id_for_season(2016)
    extra_run_per_team = {}
    with open('deliveries.csv', 'r') as deliveries_file:
        deliveries = csv.DictReader(deliveries_file)

        for delivery in deliveries:
            # print(delivery)
            if delivery['match_id'] in match_ids:
                if delivery['bowling_team'] in extra_run_per_team:
                    extra_run_per_team[delivery['bowling_team']] += int(delivery['extra_runs'])
                else:
                    extra_run_per_team[delivery['bowling_team']] = int(delivery['extra_runs'])
    return extra_run_per_team

# print(extra_run_per_team)
def charting():
    extra_runs_per_team = extra_runs_scored_per_team()
    x_axis =[]
    y_axis = []
    for team, runs in extra_runs_per_team.items():
        x_axis.append(team)
        y_axis.append(runs)

    plot_data('teams', 'extra runs', 'extra runs conceded per team in 2016', x_axis, y_axis)
    plt.xticks(rotation='vertical')
    show_plot()

def execute():
    charting()

execute()
