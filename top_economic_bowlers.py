"""For the year 2015 plot the top economical bowlers."""

import csv
from utilities import match_id_for_season
from utilities import plot_data, show_plot
from matplotlib import pyplot as plt
plt.style.use('fivethirtyeight')


match_ids_2015 = match_id_for_season(2015)

def bowler_economy_calculation():
    bowler_economy = {}
    with open('deliveries.csv', 'r') as deliveries_file:
        deliveries = csv.DictReader(deliveries_file)

        for delivery in deliveries:
            if (delivery['match_id'] in match_ids_2015) and (delivery['is_super_over'] == '0') :
                bowler_name = delivery['bowler']
                runs_given = int(delivery['total_runs'])
                bye_runs = int(delivery['bye_runs'])
                legbye_runs = int(delivery['legbye_runs'])
                if bowler_name in bowler_economy:
                    bowler_economy[bowler_name]['runs'] += (runs_given - bye_runs - legbye_runs)
                    bowler_economy[bowler_name]['balls'] += 1
                else:
                    bowler_economy[bowler_name] = {'runs':(runs_given - bye_runs - legbye_runs), 'balls': 1}
    return bowler_economy


def charting():
    bowler_economy = bowler_economy_calculation()
    economy_per_bowler = {}
    for bowler, run_per_ball in bowler_economy.items():
        economy = (run_per_ball['runs']*6)/run_per_ball['balls']
        economy_per_bowler[economy] = bowler

    top_10 = 10
    top_10_economy = []
    top_10_bowler = []
    while top_10 > 0:
        for economy, bowler in sorted(economy_per_bowler.items()):
            top_10_economy.append(economy)
            top_10_bowler.append(bowler)
            del economy_per_bowler[economy]
            break
        top_10 -= 1
    # print(top_10_bowler, top_10_economy)
    plot_data('Player', 'Economy', 'Top economic bowlers of 2015', top_10_bowler, top_10_economy)
    plt.xticks(rotation='vertical')
    show_plot()


def execute():
    charting()

execute()